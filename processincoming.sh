#!/bin/bash

# Process incoming packages
reprepro -b /var/opt/repo-debpm/ --waitforlock 100 processincoming main

# Hack, but currently the best solution. Building _all packages and _any packages seperately makes problems with our caf solution
for i in ../repo-debpm-incoming/*.dsc; do reprepro includedsc bullseye $i; done
reprepro includedeb bullseye ../repo-debpm-incoming/*.deb

# export
reprepro dumpunreferenced
reprepro deleteunreferenced
reprepro export

# Symlink to distributions to suites
reprepro createsymlinks

# Enable again once it's certain why some packages are not being included
rm -rf ../repo-debpm-incoming/*

# Generate appstream metadata
appstream-generator run testing

