import subprocess
import random
import string

class Docker:
    container_image: str = ""
    container_name: str = ""

    @staticmethod
    def __docker(command: list):
        subprocess.call(["docker"] + command)

    def __init__(self, container_image: str):
        self.container_image = container_image
        self.container_name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))

        self.__docker(["run", "-t", "-d", "--name", self.container_name, self.container_image])
        self.__docker(["start", self.container_name])

    def __del__(self):
        self.__docker(["stop", self.container_name])
        self.__docker(["rm", self.container_name])

    def call(self, command: list):
        return subprocess.call(["docker", "exec", self.container_name] + command)

    def check_output(self, command: list) -> bytes:
        return subprocess.check_output(["docker", "exec", self.container_name, ] + command)
