#!/usr/bin/env python3

from docker import Docker

image = "registry.gitlab.com/debian-pm/tools/build:testing-amd64"

container = Docker(image)

print("Setting up container comparable to rootfs...")
container.call(["wget", "-q", "https://gitlab.com/debian-pm/tools/rootfs-builder-debos/raw/master/scripts/pin-debian-pm-repository.sh"])
container.call(["bash", "pin-debian-pm-repository.sh"])
container.call(["rm", "pin-debian-pm-repository.sh"])

# Download list of packages
container.check_output(["apt", "update", "-o", "Dir::Etc::SourceList=/etc/apt/sources.list.d/debian-pm.list"])

# extract
apt_list_out: str = container.check_output(["apt", "list"]).decode().strip()
packages: list = []
for line in apt_list_out.split("\n"):
    if line != "" and line != "Listing..." and not "-dbgsym" in line:
        packages.append(line.split("/")[0])

print("I: Avalilable packages in debian-pm as of now...")
print(packages)

print("I: Trying to install the packages")
container.call(["apt", "update"])
container.call(["apt", "install", "--dry-run"] + packages)

del container
